class WhileTest {
    public static void main(String[] args) {
        int a = 4;
        while (a > 0) {
            System.out.println(a);
            a = a - 1;
        }
    }
}